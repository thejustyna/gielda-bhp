$(document).foundation();

$(document).ready(function(){

var topBarHeight = $('.top-bar').height();
console.log(topBarHeight);

// var $nav = $('.greedy-nav');
// var $btn = $('#testbtn');
// var $vlinks = $('.greedy-nav .visible-links');
// var $hlinks = $('.greedy-nav .hidden-links');

// var breaks = [];

// function updateNav() {

//   var availableSpace = $btn.hasClass('hidden') ? $nav.width() : $nav.width() - $btn.width() - 30;

//   // The visible list is overflowing the nav
//   if($vlinks.width() > availableSpace) {

//     // Record the width of the list
//     breaks.push($vlinks.width());

//     // Move item to the hidden list
//     $vlinks.children().last().prependTo($hlinks);

//     // Show the dropdown btn
//     if($btn.hasClass('hidden')) {
//       $btn.removeClass('hidden');
//     }

//   // The visible list is not overflowing
//   } else {

//     // There is space for another item in the nav
//     if(availableSpace > breaks[breaks.length-1]) {

//       // Move the item to the visible list
//       $hlinks.children().first().appendTo($vlinks);
//       breaks.pop();
//     }

//     // Hide the dropdown btn if hidden list is empty
//     if(breaks.length < 1) {
//       $btn.addClass('hidden');
//       $hlinks.addClass('hidden');
//     }
//   }

//   // Keep counter updated
//   $btn.attr("count", breaks.length);

//   // Recur if the visible list is still overflowing the nav
//   if($vlinks.width() > availableSpace) {
//     updateNav();
//   }

// }

// // Window listeners

// $(window).resize(function() {
//     updateNav();
// });

// $btn.on('click', function() {
//   $hlinks.toggleClass('hidden');
// });

// updateNav();



// $('.off-canvas.is-transition-overlap.is-open').css("top", ""+topBarHeight+"px");

//--- SYSTEM OPINII - DO DOPRACOWANIA --- !!!!

  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });

  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });


  var addCommentPossibility = true;

  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10);
    var stars = $(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }

    // --- DODAWANIE OPINII O UZYTKOWNIKU  ---
    $('#addOpinionAboutUser').click(function(){

      var textarea = $('textarea.addOpinionAboutUserTextarea');
      var opinionText = '<p>'+textarea.val()+'</p>';

      var iconStars =  '';

      for (var i = 0; i < onStar; i++) {
        iconStars+= '<i class="icon-star"></i>';
      }

      if(opinionText&&addCommentPossibility==true) {
        setTimeout(function(){

          $('ul.userOpinionsList').append('<li>'+iconStars + opinionText + '</li>').hide().fadeIn(500);
        }, 300);

      }
      addCommentPossibility = false;
    })
  });


// DODAWANIE UŻYTKOWNIKA DO LISTY ZAINTERESOWANYCH OSÓB

  var addMetoInteresterUsersList = true;

  $('#iAmInterested').click(function(){

    var interestedUser = "<li><a href=\"user-profil.html\"><img class=\"smallImagePartners\"src=\"img/user4.jpg\"><p><b>Oskar Malinowski</b></p><p>WEBImpuls</p></a></li>";
    if (addMetoInteresterUsersList == true) {


      $('.interestedUsers ul').append(interestedUser);
    }
    $('#iAmInterested').fadeOut(500);
    addMetoInteresterUsersList = false;

    $( ".addCommentAsInterestedUser" ).delay(1000).slideToggle( "slow");

  });


  //DODAWANIE OPCJI ODPOWIEDZ DO OSTATNIEGO KOMENTARZA ->  OPCJA TYLKO DLA WLASCICIELA OGLOSZENIA
  $( ".singleComment:last").append("<button id=\"reply\"class=\"button small reply\">Odpowiedz</button>");

  $('#reply').click(function(){
    $( ".addCommentAsOwner").slideToggle( "slow");
  });


// DODAWANIE KOMENTARZA PO KLIKNIECIU -JESTEM ZAINTRESOWANY


var addCommentAsInterestedUser = true;
  $('#addCommentAsInterestedUserButton').click(function(){

    var text = $('textarea#addCommentText').val();
    var image = $('img#addCommentUserImage').attr('src');

    if(text && addCommentAsInterestedUser == true) {
      var test = "<div class=\"singleComment\"><a href=\"user-profil.html\"><img src=\""+image+"\" alt=\"\"></a><a href=\"user-profil.html\"><span class=\"userPublishesName\">Oskar Malinowski</span></a><p>"+text+"</p></div>";
      $('.commentsForAdvert').append(test);
    }

    addCommentAsInterestedUser = false;

  });

  // DODAWANIE ODPOWIEDZI NA KOMENARTZ/ODPOWIEDZ - OPCJA DLA WLASCICIELA OGLOSZENIA

  var addReplyAsAdvertOwner = true;
  $('#addReplyButton').click(function(){

    var text = $('textarea#addReplyText').val();
    var image = $('img#addReplyUserImage').attr('src');

    if(text && addReplyAsAdvertOwner == true) {
      var test = "<div class=\"singleComment reply\"><a href=\"user-profil.html\"><img src=\""+image+"\" alt=\"\"></a><a href=\"user-profil.html\"><span class=\"userPublishesName\">Oskar Malinowski</span></a><p>"+text+"</p></div>";
      $('.commentsForAdvert').append(test);
      $('.addCommentAsOwner').fadeOut();
    }


    $( ".singleComment").remove("<button id=\"reply\"class=\"button small reply\">Odpowiedz</button>");

    addReplyAsAdvertOwner = false;

  });

  // DATA TYLKO DO PRZODU W IINPUT
  var now = new Date(),
  minDate = now.toISOString().substring(0,10);

  $('#my-date-input').prop('min', minDate);

});
