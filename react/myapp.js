
var obj = { "table":"customers", "limit":20 };
var dbParam = JSON.stringify(obj);
console.log(dbParam);

// Hello World + Introducing JSX
function formatName(user) {
	return user.firstName + ' ' + user.lastName;
}

const user = {
	firstName: 'Justyna',
	lastName: 'Lezuch'
};


// Zbudowanie elementu za pomocą funkcji reacta
const category = React.createElement(
	'div',
	{className: 'label label3'},
	'kategoria'
	);


const advert = (

	<div className="advert">
	<span className="postMoreOptions">
	<a href=""><i className="icon-star"></i></a>
	<a href=""><i className="icon-ellipsis-vert"></i></a>
	</span>

	<img src="img/user1.jpg" className="userSmallImage"/>
	<span className="postData">
	<span className="userPublishesName"> Aleksander Baj</span>
	<span> <i class="fi-clock"></i> 25.02.2018 </span>
	</span>

	<div className="labelWrapper">
	{category}
	</div>

	<p><b>Ładowane Reactem </b>Witam potrzebuje wykonawcy szkoleń początkowych BHP na stanowiska kasjer w sieci sklepów w małoposlce. Do przeszkolenia jest około 70 pracowników w ciagu 3 tygodni w różnych miejscach małopolski. </p>

	<div className="postImportantInfo">
	<span><i className="fi-clock"></i> 20.05.2018</span>
	<span><i className="fi-marker"></i> Kraków, małopolskie</span>
	<span><i className="fi-map"></i> WEBImpuls </span>
	<span><i className="fi-dollar"></i> 3000zl</span>
	</div>
	<a href="advertisement_details.html" className="button small">Jestem zainteresowany</a>
	</div>

	);


	ReactDOM.render(
	advert,
	document.getElementById('root')
	);

	// Rendering Elements
	function tick() {
		const rendering = (
			<div>
			<h1>Witaj, świecie</h1>
			<h2>It is {new Date().toLocaleTimeString()}</h2>
			</div>
			);
			ReactDOM.render(
			rendering,
			document.getElementById('rendering')
			);
		}

		setInterval(tick, 1000);

		// Components and Props
// Function
function Welcome(props) {
	return (
		<h1>Hello, {props.name}</h1>
		);
	}
	// Class
	class WelcomeClass extends React.Component {
		render() {
			return (
				<h1>Hello, {this.props.name}</h1>
				);
			}
		}
		// Rendering
		const element1 = <div />
		const element2 = <Welcome name="Sara" />;
		const element3 = <WelcomeClass name="SaraClass" />;

		function App() {
			return (
				<div>
				<Welcome name="Sara" />
				<WelcomeClass name="CahalClass" />
				<WelcomeClass name="EditeClass" />
				</div>
				);
			}
			const componentsProps = (
			<div>
			{element1}
			{element2}
			{element3}
			<App />
			</div>
			);

			ReactDOM.render(
			componentsProps,
			document.getElementById('componentsProps')
			);

